#!/bin/bash

DIR="$HOME/.config/quoter"
mkdir -p "$DIR"
CNF_FILE="$DIR/quoter.conf"

notify(){
source <(grep = $CNF_FILE)
quotes=${quotes_file:-"/usr/share/mabox/quotes.txt"}
Q=$(shuf -n 1 "${quotes}")
QUOTE=${Q%|*}
AUTOR=${Q#*|}
notify-send.sh -t 15000 --icon=$HOME/.config/mabox/wpicon.png "${AUTOR}\n" "${QUOTE}" --action="Next:quote.sh -n"
}

conky() {

if [ ! -f $CNF_FILE ]; then
cat <<EOF > ${CNF_FILE}
quote_fontsize=12
author_fontsize=10
font_name="Ubuntu"
EOF
fi
source <(grep = $CNF_FILE)
qfsize=${quote_fontsize:-12}
afsize=${author_fontsize:-10}
fontname=${font_name:-"Source Code Pro"}
quotes=${quotes_file:-"/usr/share/mabox/quotes.txt"}


Q=$(shuf -n 1 "${quotes}")


QUOTE=${Q%|*}
AUTOR=${Q#*|}
#QUOTE=$(crow -b $(echo $Q | jq .quoteText))
#AUTOR=$(echo $Q | jq .quoteAuthor | tr -d '"')
#echo "${QUOTE}|${AUTOR}" >> "${DIR}"/forismatic.txt

#QUOTE="Kto to jest Hiszpania? i dlaczego był Hitler?"
#AUTOR="Nieznany Zenon"

printf "\${font ${fontname}:size=$qfsize}\${color}"
echo ${QUOTE} | fmt -80
printf "\${font ${fontname}:size=$afsize}\${alignr}\${color0}"
echo "${AUTOR}"
}
case "$1" in
	-n) notify;;
	*) conky;;
esac
