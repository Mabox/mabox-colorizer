#!/bin/bash

getdata () {
df -h | grep -v "boot" | grep -v "/dev/loop" | grep '^/dev' | while read DEVICE SIZE USED FREE PERCENT MOUNT
do
[[ "${#MOUNT}" -gt "10" ]] && MOUNTLBL=${MOUNT##*/} || MOUNTLBL=${MOUNT}
[[ "${#MOUNTLBL}" -gt "10" ]] && MOUNTLBL=${MOUNTLBL:0:9}
echo "$MOUNTLBL\${goto 90}\${color2}\${fs_bar 6,64 $MOUNT}\${color}\${goto 160}$USED\${goto 200}$PERCENT\${alignr}$SIZE"
done
}
case "$LANG" in
    pl*)
    MOUNTPOINTS="Punkty montowania"
    USED="użyte"
    SIZE="rozmiar"
    ;;
    *)
    MOUNTPOINTS="MOUNT POINTS"
    USED="used"
    SIZE="size"
    ;;
esac
echo "$MOUNTPOINTS"
echo "\${goto 160}$USED\${goto 200}%\${alignr}$SIZE"
getdata
